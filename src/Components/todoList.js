import React, { Component } from "react";
import TodoItem from "./todoItem"

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem 
                key={todo.id}
                title={todo.title}
                completed={todo.completed} 
                id={todo.id}
                handleChecked={this.props.handleChecked}
                handleDelete={this.props.handleDelete}
              />
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList