import React, { Component } from "react";
import "./index.css";
import TodoList from "./Components/todoList" 
import { Route, NavLink } from "react-router-dom";
import { connect } from 'react-redux';
import { addTodo, clearCompletedTodos, deleteTodo, toggleTodo } from './actions';

class App extends Component {

  handleChecked = (event, todoIdToDelete) => {
    this.props.toggleTodo(todoIdToDelete)
  }

  handleDelete = (event, todoIdToDelete ) => {
    this.props.deleteTodo(todoIdToDelete)
  }

  handleDeleteCompleted = (event) => {
    this.props.clearCompletedTodos()
  }

  handleAddTodo = (event) => {
    if (event.key === 'Enter') {
      this.props.addTodo(event.target.value)
      event.target.value = ''
    }
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            onKeyDown={this.handleAddTodo}
            autoFocus
          />
        </header>
        <Route
          exact  
          path="/" 
          render={() => (
            <TodoList 
              todos={this.props.todos} 
              handleChecked={this.handleChecked}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <Route 
          path="/active"
          render={() => (
            <TodoList 
              todos={this.props.todos.filter(todo => todo.completed === false)} 
              handleChecked={this.handleChecked}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <Route 
          path="/completed"
          render={() => (
            <TodoList 
              todos={this.props.todos.filter(todo => todo.completed === true)} 
              handleChecked={this.handleChecked}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
          <strong>
          {this.props.todos.filter(todo => todo.completed === false).length
          }
          </strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>
          <button onClick={this.handleDeleteCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    todos: state.todos
  }
}

const mapDispatchToProps = {
 addTodo,
 clearCompletedTodos,
 deleteTodo,
 toggleTodo
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
