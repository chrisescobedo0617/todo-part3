import todosList from "./todos.json";
import { ADD_TODO, TOGGLE_TODO, DELETE_TODO, CLEAR_TODOS } from "./actions"

const initialState = {
    todos: todosList
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            //making a copy of the original array
            const newTodos = state.todos.slice()
            //modifying the new array
            newTodos.push(action.payload)
            //updating original array
            return { todos: newTodos}
        case TOGGLE_TODO: {
            //making a copy of the original array
            const newTodos = state.todos.slice()
            //modifying the new array
            const newerTodo = newTodos.map(todo => {
                if (todo.id === action.payload) {
                todo.completed = !todo.completed
            }
            return todo
        })
            //updating the original array
            return { todos: newerTodo }
        }
        case DELETE_TODO: {
            const newTodoToDelete = state.todos.filter(
                todo => todo.id !== action.payload
              )
              return { todos: newTodoToDelete }
        }
        case CLEAR_TODOS: {
            const newTodosToDelete = state.todos.filter(
                todo => todo.completed === false
              )
             return  { todos: newTodosToDelete }
        }
        default: 
        return state;
    }
}

export default reducer;